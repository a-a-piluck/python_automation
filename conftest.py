import pytest
from appium.webdriver.webdriver import WebDriver
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture(scope="function")
def browser():
    browser: WebDriver = webdriver.Firefox()
    yield browser
    browser.quit()

    # grid_url = "http://localhost:4444/wd/hub"
    # desired_caps = DesiredCapabilities.CHROME
    # browser = webdriver.Remote(desired_capabilities=desired_caps, command_executor=grid_url)


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    """ Collect result and add a comment with test status to a JIRA ticket on test failure """
    outcome = yield
    rep = outcome.get_result()
    print("Test Result: {0}".format(rep.outcome))
    print("JIRA Ticket: {0}".format(item.get_closest_marker("jira_ticket").args[0]))
    # TODO - add comment to the ticket via HTTP API
    # TODO - add test name. Search in 'outcome' variable
