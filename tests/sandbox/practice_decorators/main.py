from tests.sandbox.practice_decorators.decorators import testrail_update_status


@testrail_update_status
def greet(name):
    return f"Hi {name}"

# greet("Artur")
print(greet("Artur"))


# Живой пример декоратора
# https://pypi.org/project/pytest-testrail/