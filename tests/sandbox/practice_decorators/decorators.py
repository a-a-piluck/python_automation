import functools


def testrail_update_status(myfunction):
    @functools.wraps(myfunction)
    def wrapper_do_twice(*args, **kwargs):
        print("BEFORE")
        myfunction(*args, **kwargs)
        print("Sending HTTP request for Case ....")
    return wrapper_do_twice
