# A simple generator function
def my_gen2():
    n = 1
    print('This is printed first')
    # Generator function contains yield statements
    yield n

    n += 1
    print('This is printed second')
    yield n

    n += 1
    print('This is printed at last')
    yield n


if __name__ == '__main__':
    # result = my_gen2()
    # print(result)
    # Using for loop
    for item in my_gen2():
        print(item)


